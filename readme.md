# I Service systemd
    # 1 Intro
        * afficher le nombre de services systemd dispos sur la machine 
        * sudo systemctl list-unit-files --type=service  | cut -d' ' -f1 | wc -l

        * afficher le nombre de services systemd actifs et en cours d'exécution ("running") sur la machine

        * sudo systemctl list-units --all --type=service --state=running | grep "units listed" | cut -d' ' -f1

        * afficher le nombre de services systemd qui ont échoué ("failed") ou qui sont inactifs ("exited") sur la machine

        * sudo systemctl list-units --all --type=service --state=failed --state=exited | grep "units listed" | cut -d' ' -f1

        * afficher la liste des services systemd qui démarrent automatiquement au boot ("enabled")

        * sudo systemctl list-unit-files --type=service | grep "enabled"
    # 2 Analyse d'un service
        * Etudiez le service nginx.service
        *  systemctl status nginx
        * path nginx.service
            */usr/lib/systemd/system/nginx.service

        * afficher son contenu et expliquer les lignes qui comportent : 
            * sudo systemctl cat nginx

        * ExecStart=/usr/sbin/nginx -g 'daemon on; master_process on;' 

            *Indique le chemin à exécuter pour démarrer le service

        * ExecStartPre=/usr/sbin/nginx -t -q -g 'daemon on; master_process on;'

            *Sert à éxécuter certaines commandes additionnelles en pre ou post ExecStart

        * PIDFile=/run/nginx.pid

            * Fichier ou est stocké le pid

        * Type=forking

            *Sert à connaitre le type du service

        * ExecReload=/usr/sbin/nginx -g 'daemon on; master_process on;' -s reload

            Commandes à exécuter pour reload le service.

        * Description=A high performance web server and a reverse proxy server

            Description service

        * After=network.target

            * Lancement les services ci dessus apres le = 

        * Listez tous les services qui contiennent la ligne WantedBy=multi-user.target

            * grep -r -e 'WantedBy=multi-user.target' /usr/lib/systemd/system /etc/systemd/system /run/systemd/system /usr/lib/systemd/system
        
    # 3 Création d'un service
        A # Serveur Web
```
            [Unit]
            Description=descrptiooooooooooooooon
            After=network.target

            [Service]
            User=nginx
            Environment="WEB_PORT=8080"
            ExecStartPre=+/usr/bin/firewall-cmd --add-port=80/tcp
            ExecStart=/usr/bin/python3 -m http.server 80
            ExecStop=+/usr/bin/firewall-cmd --remove-port= 80/tcp

            [Install]
            WantedBy=multi-user.target`
```
        * prouver qu'il est en cours de fonctionnement pour systemd
            `sudo systemctl status web
            ? web.service - Web server description
            Loaded: loaded`
        * faites en sorte que le service s'allume au démarrage de la machine
            * systemctl enable web
        * prouver que le serveur web est bien fonctionnel
            * curl 127.0.0.1:8080

    # B Sauvegarde

        * Créez une unité de service qui déclenche une sauvegarde avec votre script
            *  Création fichier unité de service 
```
            [Unit]
            Description=Backup

            [Service]
            User=backup
            PIDFile=/run/backup.pid
            ExecStartPre=/usr/bin/rm -f /run/backup.pid
            ExecStartPre=/usr/bin/sh /srv/backup_scripts/post.sh
            ExecStart=/usr/bin/sh /srv/backup_scripts/backup.sh /srv/site1
            ExecStart=/usr/bin/sh /srv/backup_scripts/backup.sh /srv/site2
            ExecStartPost=/usr/bin/sh /srv/backup_scripts/post.sh

            [Install]
            WantedBy=multi-user.target
```
        * Ecrire un fichier .timer systemd
```
            [Unit]
            Description=backup every hour

            [Timer]
            OnBootSec=0min
            OnCalendar=0/1:00:00
            Unit=one-hour.service

            [Install]
            WantedBy=timers.target
```





# II Autres features

            # 1 Gestion de boot

            * systemd-analyze plot > plot.svg
            * les 3 services les plus long à démarrer sont ssh-keygen , sshd.service et tuned.service

            # 2 Gestion de l'heure

            * timedatectl 
            * fuseau horaire : Time zone: UTC (UTC, +0000)
            * NTP service: active
            * timedatectl set-timezone Europe/Paris

            # 3 Gestion des noms et de la résolution de noms

            *  Static hostname: node1.tp3.b2
            * Changer hostname
            * hostnamectl set-hostname new.node1.tp3.b2